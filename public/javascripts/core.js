var login = angular.module('login', []);

function mainController($scope, $http){
	$scope.formData = {};

	$scope.login = function(uname){
		$http.get('/api/login/'+uname)
		.success(function(data){
				if(typeof data[0] != 'undefined'){
					if(data[0].pw == $('#pw').val()){
						$scope.result = 'You are a granted user !';
					}else{
						$scope.result = 'You are not a granted user !';
					}
				}else{
					$scope.result = 'You are not a granted user !';
				}

		})
		.error(function(data){
			console.log("Err : "+data);
		});
	}

	$scope.register = function(){
		$http.post('/api/register',$scope.formData)
		.success(function(data){
			$scope.uname = $('#uname').val();
			$scope.pw = $('#pw').val();
		})
		.error(function(data){
			console.log("Err : "+data);
		});

		$scope.result = 'Successfully Registered !';
	}
}
