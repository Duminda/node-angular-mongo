
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/userdb');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

require('./routes/routes.js');

var login  = require('./routes/model/login');
	
	app.get('/api/login/:uname', function(req, res){

		login.find({uname: req.params.uname},function(err, loginData){
			if(err) res.send(err);
			res.json(loginData);
		});
	});

	app.post('/api/register', function(req, res){
	
		login.create({
			uname : req.body.uname,
			pw : req.body.pw
		},function(err, loginData){
			if(err) res.send(err);
		});
	});


app.get('/', routes.index);
app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
