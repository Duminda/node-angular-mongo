var login  = require('./model/login');

module.exports = function(app){
	
	app.get('/api/login', function(req, res){

		login.find(function(err, loginData){
			if(err) res.send(err);
			res.json(loginData);
		});
	});

	app.post('/api/register', function(req, res){
	
		login.create({
			uname : req.body.uname,
			pw : req.body.pw,
			done : req.body.result
		},function(err, loginData){
			if(err) res.send(err);
		});
	});
};

